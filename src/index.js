import React from 'react'
import ReactDOM from 'react-dom'

class LoggingButton extends React.Component {
  constructor (props) {
    super(props)
    this.handleClick()
  }

  handleClick () {
    console.log('this 是否存在:', !!this);
  }

  render() {
    return (
      <button onClick={this.handleClick}>
        Click me
      </button>
    )
  }
}

ReactDOM.render(
  <LoggingButton />,
  document.getElementById('root')
)
