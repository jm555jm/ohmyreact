export function scope () {
  let i = 0
  while (i < 10) {
    const x = 6
    let y = 6
    var z = 8
    console.log(x, y, z)
    i += 1
  }
  console.log(x, y, z)
}

export function bitwise (arg1, callback) {
  let a = arg1 || '1'

  if (a === 1 || a == 1 || a >= 1 || a > 1) {
    console.log(a)
  }

  typeof callback === 'function' && callback()
}

export function check () {
  let x = 0
  let y = ''
  let z = false
  let obj = {
    'a': x,
    'b': y,
    'c': z
  }
  if (obj['a']) { // 0
    console.log('不會進來')
  }
  if (obj['b']) { // ''
    console.log('不會進來')
  }
  if (obj['c']) { // false
    console.log('不會進來')
  }
  if (obj['d']) { // undefined
    console.log('不會進來')
  }

  let j = 'j'
  let jHasValue = !!j
  jHasValue = true
  console.log(jHasValue)

  let k = ''
  let kHasValue = !!k
  kHasValue = false
  console.log(kHasValue)
}

export function array () {
  let now
  let arr
  let brr
  let crr
  let x

  now = '加入元素到最後'
  arr = ['0']
  arr.push('a')
  arr = ['0', 'a']

  now = '移除並回傳最後一個元素'
  x = arr.pop()
  arr = ['0']
  x = 'a'

  now = '加入元素到最前面'
  arr.unshift('b')
  arr = ['b', '0']

  now = '移除並回傳第一個元素'
  x = arr.shift()
  arr = ['0']
  x = 'b'

  now = '陣列複製'
  brr = arr.slice()
  now = '刪除指定元素'
  brr.splice(0, 1) // 從0的位置刪除1個

  now = '陣列合併1'
  arr = ['1', '2', '3']
  brr = ['4', '5', '6']
  crr = arr.concat(brr)
  crr = ['1', '2', '3', '4', '5', '6']

  now = '陣列合併2'
  crr = [
    '-2', '-1', '0',
    ...arr,
    '7', '8', '9',
    ...brr
  ]
  crr = [
    '-2', '-1', '0',
    '1', '2', '3',
    '7', '8', '9',
    '4', '5', '6'
  ]

  now = '尋找'
  x = crr.indexOf('4')
  x = 3
  x = crr.indexOf('8')
  x = -1

  now = 'map1'
  arr = [1, 2, 3]
  brr = []
  arr.map(function (value, index) {
    brr[index] = value * 2
  })
  brr = [2, 4, 6]

  now = 'map2'
  arr = [1, 2, 3]
  brr = arr.map(function (value, index) {
    return value * 2
  })
  brr = [2, 4, 6]

  now = 'map3'
  arr = [1, 2, 3]
  brr = arr.map((value, index) => {
    return value * 2
  })
  brr = [2, 4, 6]

  now = 'map4'
  arr = [1, 2, 3]
  brr = arr.map(value => value * 2)
  brr = [2, 4, 6]

  console.log(now, x)
}

export function object () {
  let now
  let obj
  let x

  now = '加入一個key-value'
  obj = {
    'key1': 'value1'
  }
  obj['key2'] = 'value2'
  obj = {
    'key1': 'value1',
    'key2': 'value2'
  }

  now = '刪除一個key-value'
  delete obj['key2']

  now = 'assign1'
  obj = {
    'key1': 'value1',
    'key2': 'value2'
  }

  now = 'assign2'
  let value1 = 'value1'
  let value2 = 'value2'
  obj = {
    'key1': value1,
    'key2': value2
  }

  now = 'assign3'
  let key1 = 'value1'
  let key2 = 'value2'
  obj = {
    key1,
    key2
  }

  now = 'assign4'
  const KEY_1 = 'key1'
  const KEY_2 = 'key2'
  const VALUE_1 = 'value1'
  obj = {
    [KEY_1]: VALUE_1,
    [KEY_2]: 'value2'
  }

  now = 'assign5'
  let obj1 = {
    'x': 'a',
    'y': 'b'
  }
  let obj2 = {
    'y': 'c',
    'z': 'd'
  }
  obj = {
    'x': '1',
    'y': '2',
    ...obj1,
    ...obj2,
    'w': '5'
  }
  obj = {
    'x': 'a',
    'y': 'c',
    'z': 'd',
    'w': '5'
  }

  now = 'assign6'
  let obj3 = {
    'x': 'a',
    'y': 'b'
  }
  let obj4 = {
    'y': 'c',
    'z': 'd'
  }
  obj = {
    ...obj3,
    'y': {
      ...obj4
    }
  }
  obj = {
    'x': 'a',
    'y': {
      'y': 'c',
      'z': 'd'
    }
  }

  now = '複製object'
  const obj5 = JSON.parse(JSON.stringify(obj))

  console.log(now, x, obj5)
}

export function forloop () {
  let now
  let str
  let arr = ['a', 'b', 'c']
  let obj = {
    'x': 'a',
    'y': 'b',
    'z': 'c'
  }

  now = 'array'
  for (const y of arr) {
    console.log(y)
    str = 'a, b, c'
  }
  for (const y in arr) {
    console.log(y)
    str = '0 ,1, 2'
  }

  now = 'object'
  for (const y of Object.keys(obj)) {
    console.log(y)
    str = 'x ,y, z'
    console.log(obj[y])
    str = 'a, b, c'
  }
  for (const y in obj) {
    console.log(y)
    str = 'x ,y, z'
    console.log(obj[y])
    str = 'a, b, c'
  }

  console.log(now, str)
}

export function asyncFunc1 () {
  api1(() => {
    api1(() => {
      api1(() => {
        console.log('成功')
      }, (err) => {
        console.log(err)
      })
    }, (err) => {
      console.log(err)
    })
  }, (err) => {
    console.log(err)
  })

  api2()
    .then(() => {
      return api2()
    })
    .then(() => {
      return api2()
    })
    .then(() => {
      console.log('成功')
    })
    .catch((err) => {
      console.log(err)
    })
}

export async function asyncFunc2 () {
  try {
    await api2()
    await api2()
    await api2()
    console.log('成功')
  } catch (err) {
    console.log(err)
  }
}

function api1 (onSuccess, onFailed) {
  let err = false
  setTimeout(() => {
    if (err) {
      onFailed(err)
    } else {
      onSuccess()
    }
  }, 2000)
}
function api2 () {
  let err = false
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    }, 2000)
  })
}

export default function main () {
  console.log('main')
}
