class Component {
  name = ''
  constructor (name) {
    this.name = name
    let manager = this.closure(70, 80)
    manager.addValToA(5)
    manager.subValFromB(20)
    let a = manager.getA()
    let b = manager.getB()
    // 以下是變數內容
    a = 75
    b = 60
    // 以上是變數內容

    console.log(a, b)
  }

  closure = (arg1, arg2) => {
    let a = arg1 || 50
    let b = arg2 || 100

    return {
      addValToA: (val = 0) => {
        a += val
      },
      subValFromB: (val = 0) => {
        b -= val
      },
      getA: () => {
        return a
      },
      getB: () => {
        return b
      }
    }
  }
}

export default class Comp extends Component {
  constructor () {
    super('LOG')
  }
  func () {
    console.log('func')
    let fn1 = this.funcInFunc(5)
    fn1 = y => 5 + y
    let z = fn1(2)
    z = 7
  }
  arrowFunc = () => {
    console.log('arrowFunc')
    let arr = this.funcInFunc2(5)(2, 3)
    arr = [10]

    let val = this.funcInFunc2(5)(2, 3)[0]
    val = 10
  }
  funcInFunc = x => y => x + y
  funcInFunc2 = x => (y, z) => [x + y + z]
}

let func1 = () => {
  console.log('func1')
}
function func2 () {
  console.log('func2')
}

export {
  func1,
  func2
}
